import jwt from "jsonwebtoken";

export const createToken = (data) => {
  let token = jwt.sign({ data }, "BIMAT", {
    algorithm: "HS256",
    expiresIn: "5y",
  });
  return token;
};

export const checkToken = (token) => {
  return jwt.verify(token, "BIMAT");
};

export const decodeToken = (token) => {
  return jwt.decode(token);
};

export const checkApi = (req, res, next) => {
  try {
    let { token } = req.headers;
    checkToken(token);
    next();
  } catch (error) {
    res.status(401).send("Không có quyền tuy cập");
  }
};
