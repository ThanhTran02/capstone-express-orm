import express from "express";
import {
  getInfoUser,
  updateUser,
  uploadAvatar,
} from "../controllers/userController.js";
import { upload } from "../controllers/uploadController.js";
import { checkApi } from "../config/jwt.js";

const userRoute = express.Router();
userRoute.put("/account-update", checkApi, updateUser); //done
userRoute.get("/account", checkApi, getInfoUser); //done
userRoute.put("/upload-avatar", checkApi, upload.single("file"), uploadAvatar); //done
export default userRoute;
