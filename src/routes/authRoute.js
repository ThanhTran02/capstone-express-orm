import express from "express";
import { loginUser, signUpUser } from "../controllers/authController.js";

const authRoute = express.Router();
authRoute.post("/login", loginUser); //done
authRoute.post("/sign-up", signUpUser); //done
export default authRoute;
