import express from "express";
import {
  createImage,
  deleteImageById,
  getImage,
  getImageById,
  getImageByName,
  getImageCreateByUserId,
  getImageSaved,
  getImageSavedByUserId,
} from "../controllers/imageController.js";
import { checkApi } from "../config/jwt.js";
import { upload } from "../controllers/uploadController.js";

const imageRoute = express.Router();

imageRoute.get("/get-all-img", getImage); //done
imageRoute.get("/get-img-by-name/:name", getImageByName); //done
imageRoute.get("/get-img-by-id/:imageId", getImageById); //done
imageRoute.get("/get-img-save/:userId", getImageSavedByUserId); //done
imageRoute.get("/get-img-create/:userId", getImageCreateByUserId); //done
imageRoute.get("/get-img-saved/:imageId", checkApi, getImageSaved); //done

imageRoute.post("/create", checkApi, upload.single("file"), createImage); //done
imageRoute.put("/delete/:imageId", checkApi, deleteImageById); //done

export default imageRoute;
