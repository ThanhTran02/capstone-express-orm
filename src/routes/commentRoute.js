import express from "express";
import {
  getCommentByImgId,
  postComment,
} from "../controllers/commentController.js";
import { checkApi } from "../config/jwt.js";

const commentRoute = express.Router();

commentRoute.get("/get-comment/:imageId", getCommentByImgId); //donev
commentRoute.post("/add-comment", checkApi, postComment); //done
export default commentRoute;
