import { PrismaClient } from "@prisma/client";
import { decodeToken } from "../config/jwt.js";
const prisma = new PrismaClient();

//lay danh sach anh
export const getImage = async (req, res) => {
  try {
    let data = await prisma.hinh_anh.findMany({
      where: {
        exist: false,
      },
    });
    res.status(200).send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
// lay dánh sach theo ten
export const getImageByName = async (req, res) => {
  const { name } = req.params;
  try {
    let data = await prisma.hinh_anh.findMany({
      where: {
        ten_hinh: {
          contains: name,
        },
        exist: false,
      },
    });

    if (data.length) {
      res.status(200).send(data);
    } else {
      res.status(404).send({ error: "Photo not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};

export const getImageSaved = async (req, res) => {
  const { imageId } = req.params;
  try {
    const { token } = req.headers;
    const infoUser = decodeToken(token);
    const { nguoi_dung_id } = infoUser.data.checkEmail;
    const data = await prisma.luu_anh.findMany({
      where: {
        hinh_id: parseInt(imageId),
        nguoi_dung_id,
        exist: false,
      },
    });

    if (data.length) {
      res.status(200).send(data);
    } else {
      res.status(404).send({ error: "Photo not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};

//lay thong tin anh theo id
export const getImageById = async (req, res) => {
  const { imageId } = req.params;
  try {
    const data = await prisma.hinh_anh.findMany({
      where: {
        hinh_id: parseInt(imageId),
        exist: false,
      },
      include: {
        nguoi_dung: true,
      },
    });
    if (data.length) {
      res.status(200).send(data);
    } else {
      res.status(404).send({ error: "Photo not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
//lay danh sach anh da luu theo id nguoi dung
export const getImageSavedByUserId = async (req, res) => {
  const { userId } = req.params;
  try {
    const data = await prisma.luu_anh.findMany({
      where: {
        nguoi_dung_id: parseInt(userId),
        exist: false,
      },
      include: {
        hinh_anh: true,
      },
    });
    if (data.length) {
      const result = {
        nguoi_dung_id: userId,
        hinh_anh: data.map((luuAnh) => luuAnh.hinh_anh),
      };
      res.status(200).send(result);
    } else {
      res.status(404).send({ error: "Photo not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};

//lay danh sach anh da tao theo id nguoi dung
export const getImageCreateByUserId = async (req, res) => {
  const { userId } = req.params;
  try {
    const data = await prisma.hinh_anh.findMany({
      where: {
        nguoi_dung_id: parseInt(userId),
        exist: false,
      },
    });
    if (data.length) {
      const result = {
        nguoi_dung_id: userId,
        hinh_anh: data.map(({ nguoi_dung_id, ...rest }) => rest),
      };
      res.status(200).send(result);
    } else {
      res.status(404).send({ error: "Photo not found" });
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};

//xoa anh theo id
export const deleteImageById = async (req, res) => {
  const { imageId } = req.params;
  try {
    await prisma.hinh_anh.update({
      where: {
        hinh_id: parseInt(imageId),
        exist: false,
      },
      data: {
        exist: true,
      },
    });
    res.status(200).send("Xoá thành công!");
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
// tạo anh của 1 ngui dung
export const createImage = async (req, res) => {
  try {
    const { mota } = req.body;

    const file = req.file;
    const { token } = req.headers;
    const { nguoi_dung_id } = decodeToken(token).data.checkEmail;
    const data = {
      ten_hinh: file.filename,
      duong_dan: file.destination.substring(13),
      mo_ta: mota,
      nguoi_dung_id,
    };
    await prisma.hinh_anh.create({ data });
    res.status(200).send("Tạo thành công!");
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
