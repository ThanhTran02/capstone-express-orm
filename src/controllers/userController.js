import { PrismaClient } from "@prisma/client";
import { decodeToken } from "../config/jwt.js";
import { toiUuHinh } from "./uploadController.js";

const prisma = new PrismaClient();
//update nguoi dung done
export const updateUser = async (req, res) => {
  try {
    const { ho_ten, tuoi } = req.body;
    const { token } = req.headers;
    const infoUser = decodeToken(token);
    const { nguoi_dung_id } = infoUser.data.checkEmail;
    const getUser = await prisma.nguoi_dung.findFirst({
      where: { nguoi_dung_id },
    });
    let updateUser = { ...getUser, ho_ten, tuoi: Number(tuoi) };
    await prisma.nguoi_dung.update({
      where: { nguoi_dung_id },
      data: updateUser,
    });
    res.status(200).send("Cập nhật thông tin thành công");
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};

//lay thong tin nguoi dung done
export const getInfoUser = async (req, res) => {
  try {
    const { token } = req.headers;
    const infoUser = decodeToken(token);
    const { nguoi_dung_id } = infoUser.data.checkEmail;
    const data = await prisma.nguoi_dung.findFirst({
      where: { nguoi_dung_id },
    });
    res.status(200).send(data);
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
// upload avatar done
export const uploadAvatar = async (req, res) => {
  let file = req.file;
  let { token } = req.headers;
  let userInfo = decodeToken(token);
  let { nguoi_dung_id } = userInfo.data.checkEmail;
  let getUser = await prisma.nguoi_dung.findFirst({ where: { nguoi_dung_id } });
  let updateUser = { ...getUser, anh_dai_dien: file.filename };
  await prisma.nguoi_dung.update({
    where: { nguoi_dung_id },
    data: updateUser,
  });
  const imgBase = toiUuHinh(file);
  res.status(200).send(file);
};
