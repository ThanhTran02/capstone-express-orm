import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";
import { createToken } from "../config/jwt.js";
const prisma = new PrismaClient();

export const loginUser = async (req, res) => {
  const { email, mat_khau } = req.body;
  try {
    let checkEmail = await prisma.nguoi_dung.findFirst({
      where: {
        email,
      },
    });
    if (checkEmail) {
      let checkPass = bcrypt.compareSync(mat_khau, checkEmail.mat_khau);
      if (checkPass) {
        let token = createToken({ checkEmail });
        res.status(200).send(token);
      } else {
        res.status(404).send("Mật khẩu không đúng!");
      }
    } else {
      res.status(400).send("Email không đúng !");
    }
  } catch (error) {
    console.log(error);
  }
};

export const signUpUser = async (req, res) => {
  let { email, mat_khau, ho_ten, tuoi } = req.body;
  try {
    const checkEmail = await prisma.nguoi_dung.findMany({
      where: { email: email },
    });
    if (checkEmail.length) {
      res.status(400).send("Email đã tồn tại !");
      return;
    }
    let newPass = bcrypt.hashSync(mat_khau, 10);
    let newData = {
      //   nguoi_dung_id: "",
      email,
      mat_khau: newPass,
      ho_ten,
      tuoi: Number(tuoi),
      anh_dai_dien: "",
    };
    await prisma.nguoi_dung.create({
      data: newData,
    });
    res.status(200).send("Đăng kí thành công!");
  } catch (error) {}
};
