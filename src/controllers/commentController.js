import { PrismaClient } from "@prisma/client";
import { decodeToken } from "../config/jwt.js";
const prisma = new PrismaClient();

// lay thong tin binh luan theo anh
export const getCommentByImgId = async (req, res) => {
  try {
    const { imageId } = req.params;
    const data = await prisma.binh_luan.findMany({
      where: {
        hinh_id: Number(imageId),
      },
    });
    data.length
      ? res.status(200).send(data)
      : res.status(404).send({ error: "Comment not found" });
  } catch (error) {
    console.error(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
// done
export const postComment = async (req, res) => {
  try {
    const { hinh_id, noi_dung } = req.body;
    const { token } = req.headers;
    const infoUser = decodeToken(token);
    const { nguoi_dung_id } = infoUser.data.checkEmail;
    var date = `${new Date().getFullYear()}-${
      new Date().getMonth() + 1
    }-${new Date().getDate()}`;
    date = new Date(date);
    await prisma.binh_luan.create({
      data: {
        nguoi_dung_id,
        hinh_id: Number(hinh_id),
        ngay_binh_luan: date,
        noi_dung,
      },
    });
    res.status(200).send("Bình luận thành công");
  } catch (error) {
    console.log(error);
    res.status(500).send({ error: "Internal server error" });
  }
};
