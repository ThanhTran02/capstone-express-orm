/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `binh_luan` (
  `binh_luan_id` int NOT NULL AUTO_INCREMENT,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  `ngay_binh_luan` date DEFAULT NULL,
  `noi_dung` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`binh_luan_id`),
  KEY `fk_nguoi_dung` (`nguoi_dung_id`),
  KEY `fk_nguoi_dung2` (`hinh_id`),
  CONSTRAINT `fk_nguoi_dung` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `fk_nguoi_dung2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hinh_anh` (
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ten_hinh` varchar(255) DEFAULT NULL,
  `duong_dan` varchar(255) DEFAULT NULL,
  `mo_ta` varchar(255) DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`hinh_id`),
  KEY `fk_hinh_anh_user` (`nguoi_dung_id`),
  CONSTRAINT `fk_hinh_anh_user` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `luu_anh` (
  `nguoi_dung_id` int NOT NULL,
  `hinh_id` int NOT NULL,
  `ngay_luu` date DEFAULT NULL,
  `exist` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`nguoi_dung_id`,`hinh_id`),
  KEY `fk_luu_anh` (`hinh_id`),
  CONSTRAINT `fk_luu_anh` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`),
  CONSTRAINT `fk_luu_anh2` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `nguoi_dung` (
  `nguoi_dung_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `mat_khau` varchar(255) DEFAULT NULL,
  `ho_ten` varchar(255) DEFAULT NULL,
  `tuoi` int DEFAULT NULL,
  `anh_dai_dien` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(1, 1, 1, '2023-10-19', 'Great photo!');
INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(2, 2, 2, '2023-10-20', 'Lovely picture!');
INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(3, 3, 3, '2023-10-21', 'Amazing shot!');
INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(11, 5, 1, '2023-10-29', 'good'),
(12, 5, 1, '2023-10-29', 'good'),
(13, 5, 1, '2023-10-29', 'good'),
(14, 5, 2, '2023-10-29', 'good'),
(15, 7, 2, '2023-11-10', 'good day'),
(16, 7, 2, '2023-11-10', 'good day');

INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`, `exist`) VALUES
(1, 'image1.jpg', '/path/to/image1.jpg', 'Beautiful landscape', 1, 0);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`, `exist`) VALUES
(2, 'image2.jpg', '/path/to/image2.jpg', 'Cute animals', 2, 0);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`, `exist`) VALUES
(3, 'image3.jpg', '/path/to/image3.jpg', 'City skyline', 3, 0);
INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`, `exist`) VALUES
(4, 'image4.jpg', '/path/to/image4.jpg', 'good', 1, 0),
(9, '1698999579289_1697959808521_img.jpg', '/public/img', '23131', 5, 0),
(10, '1699603758939_dog.png', '/public/img', '1234', 7, 0),
(11, '1699603855321_dog.png', '/public/img', 'bau troi', 7, 0);

INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`, `exist`) VALUES
(1, 2, '2023-10-21', 0);
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`, `exist`) VALUES
(2, 2, '2023-10-20', 0);
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`, `exist`) VALUES
(3, 3, '2023-10-21', 0);
INSERT INTO `luu_anh` (`nguoi_dung_id`, `hinh_id`, `ngay_luu`, `exist`) VALUES
(7, 1, '2023-10-19', 0);

INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(1, 'user1@example.com', '1', 'John Doe', 30, 'avatar1.jpg');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(2, 'user2@example.com', 'password2', 'Jane Smith', 25, 'avatar2.jpg');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(3, 'user3@example.com', 'password3', 'Mike Johnson', 35, 'avatar3.jpg');
INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(5, 'user3@example.com', '$2b$10$yv5i.4iiCayyzepyHjB/ZeSvRLdXfUMg3gRfZh61.eClHuc/Gl1qm', 'Mike', 18, '1699605473866_img.jpg'),
(7, '1234@gmail.com', '$2b$10$wOqdR6FyE4z/AwkzopVh/eiccKF000y.I2R57k1n6N.35nvPYaaci', 'Mike', 28, ''),
(8, '12345@gmail.com', '$2b$10$XqbEy6THkgbWzV4SM1frXuXqKF/eAsnCwTiOyFrUdZMpgjv19WNYm', NULL, NULL, ''),
(11, '123456@gmail.com', '$2b$10$u5kzdzZHA8MmJJojduJjVuIAXvTTiCJguLVJfsTPcdKLNUH0InnYC', '123456', 18, '');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;